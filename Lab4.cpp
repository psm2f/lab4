/*
 * Lab4.cpp
 *
 *  Created on: Feb 15, 2016
 *      Author: Patrick
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LENGTH 80

int w,h;
int index;
const char s[5] = " \t\r";//define the delimiter for the strtok function
char *token;

void headerFunc(FILE*);
int* parseFunc(FILE*);

int main() {

	FILE * pFile;

	pFile = fopen ("C:\\Users\\Patrick\\workspace\\Lab4\\src\\cat.pgm" , "r");//loading in file
	if (pFile == NULL) {				//checking file is valid
	   perror ("Error opening file");//prints error message
	   return 1;					//exits program
	}else {

		headerFunc(pFile);
		parseFunc(pFile);

	}
	fclose (pFile);

	return 0;
}

void headerFunc(FILE* pFile) {

	char *header;
	header = (char*) malloc(sizeof(double)*3);

	int *headerArray;
	headerArray = (int*) malloc(sizeof(int)*3);
	int index = 0;

	for(int i = 0; i < 3; i++) {//for the first 3 lines of the image
		fgets (header , 10 , pFile);
		if(header[strlen(header)-1]=='\n')
			header[strlen(header)-1] = '\0';
		token = strtok(header,s);//parse out the width from the header
		index++;
		headerArray[index] = atoi(token);
		//printf("The value in index %d is %i.\n",index,headerArray[index]);

		token = strtok(NULL, s);
		while( token != NULL )
		   {
				index++;
				headerArray[index] = atoi(token);
				//printf("The value in index %d is %i.\n",index,headerArray[index]);
				token = strtok(NULL, s);

		   }
	}


	w = headerArray[2];
	h = headerArray[3];

	printf("The width of this image is %d,\nThe height of the image is %d\n",w,h);


	return;
}

int* parseFunc(FILE* pFile) {

	int *tokenArray;
	tokenArray = (int*) malloc(sizeof(int)*w*h);

	char line[MAX_LENGTH];

	index = 0;
	for(int i = 0; i <= h; i++) {
		fgets (line, MAX_LENGTH , pFile);
		if(line[strlen(line)-1]=='\n')
			line[strlen(line)-1] = '\0';


		token = strtok(line, s);
		index++;
		tokenArray[index] = atoi(token);

		printf("The value in index %d is %i.\n",index,tokenArray[index]);

		token = strtok(NULL, s);
		while( token != NULL)
		{
				index++;
				tokenArray[index] = atoi(token);
				printf("The value in index %d is %i.\n\n",index,tokenArray[index]);
				token = strtok(NULL, s);


		   }
		getchar();

	}
	return tokenArray;
}





