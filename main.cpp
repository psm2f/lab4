
/*
 * Lab4.cpp
 *
 *  Created on: Feb 15, 2016
 *      Author: Patrick
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define MAX_LENGTH 100

typedef struct _PGMData {
    int row;
    int col;
    int max_gray;
    int *matrix;
} PGMData;

const char s[5] = " \t\r";//define the delimiter for the strtok function

PGMData* headerFunc(FILE*);
PGMData* parseFunc(FILE*,PGMData*);

int getPix(PGMData*, int, int);
void setPix(PGMData*, int, int, int);
int errorCheck(PGMData*,int, int);

void writeFile(PGMData*,char*);
FILE* openFile(void);

void edge(PGMData*);

int main() {

	FILE * newFile;

	/*Error check file open*/
	do {
		newFile = openFile();
	}while(newFile == NULL);

	PGMData *data = (PGMData*) malloc(sizeof(PGMData));

	data = headerFunc(newFile);
	data = parseFunc(newFile,data);

	printf("Edge Detecting...\n");
	edge(data);


	getchar();
	fclose (newFile);

	return 0;
}

void edge(PGMData* data) {

	printf("Edge ...\n");
	PGMData * mData = (PGMData*) malloc(sizeof(PGMData));

	mData->row = data->row - 1;
	mData->col = data->col - 1;
	mData->max_gray = data->max_gray;

	mData->matrix = (int*)malloc(sizeof(int)*(data->row*data->col));

	for(int x = 0; x < mData->row; x++) {
		for(int y = 0; y < mData->col; y++) {

			int magX = 0;
			int magY = 0;
			int mag = 0;

			//printf("%d - %d",x,y);

			if(errorCheck(data,x-1,y-1) == 0) {
				magX += getPix(data,x-1,y-1)*(-1);
				magY += getPix(data,x-1,y-1)*(-1);
			}else
				setPix(mData,x,y,0);
			if(errorCheck(data,x,y-1) == 0) {
				magX += getPix(data,x,y-1)*(0);
				magY += getPix(data,x,y-1)*(-2);
			}else
				setPix(mData,x,y,0);
			if(errorCheck(data,x+1,y-1) == 0) {
				magX += getPix(data,x+1,y-1)*(1);
				magY += getPix(data,x+1,y-1)*(-1);
			}else
				setPix(mData,x,y,0);
			if(errorCheck(data,x-1,y) == 0) {
				magX += getPix(data,x-1,y)*(-2);
				magY +=getPix(data,x-1,y)*(0);
			}else
				setPix(mData,x,y,0);
			if(errorCheck(data,x,y) == 0) {
				magX += getPix(data,x,y)*(0);
				magY += getPix(data,x,y)*(0);
			}else
				setPix(mData,x,y,0);
			if(errorCheck(data,x+1,y) == 0) {
				magX += getPix(data,x+1,y)*(2);
				magY += getPix(data,x+1,y)*(0);
			}else
				setPix(mData,x,y,0);
			if(errorCheck(data,x-1,y+1) == 0) {
				magX += getPix(data,x-1,y+1)*(-1);
				magY += getPix(data,x-1,y+1)*(1);
			}else
				setPix(mData,x,y,0);
			if(errorCheck(data,x,y+1) == 0) {
				magX += getPix(data,x,y+1)*(0);
				magY += getPix(data,x,y+1)*(2);
			}
			if(errorCheck(data,x+1,y+1) == 0) {
				magX += getPix(data,x+1,y+1)*(1);
				magY += getPix(data,x+1,y+1)*(1);
			}else
				setPix(mData,x,y,0);

			mag = sqrt(pow(magX,2) + pow(magY,2));
			setPix(mData,x,y,mag);
		}
	}

	char *name = (char*)malloc(sizeof(char)*50);

	printf("Edge Detect  Complete. Writing file...\n");
	printf("What would you like to name the file?\n");
	scanf("%s", name);


	writeFile(mData,name);
	free(name);
	free(mData->matrix);
	free(mData);

	return;
}

int getPix(PGMData * data, int x, int y) {

	int value = data->matrix[x*data->col+y];
	return value;
}

void setPix(PGMData * data, int x, int y, int value) {

	data->matrix[x*data->col+y] = value;
	return;
}

void writeFile(PGMData* data, char* name) {

	FILE *pgmFile;
	pgmFile = fopen(name,"w");
	if (pgmFile == NULL) {
		perror("Cannot open file to write");

		getchar();
		exit(EXIT_FAILURE);
	}

	fprintf(pgmFile, "P2 \n");
	fprintf(pgmFile, "%d %d \n", data->col, data->row);
	fprintf(pgmFile, "%d \n", data->max_gray);

	int index = 0;
	int charCounter;

	while( index <= data->col * data->row) {

		charCounter = 0;
		while(charCounter < 70) {

			if(data->matrix[index] < 10)
				charCounter += 2;
			else if(data->matrix[index] < 100)
				charCounter += 3;
			else
				charCounter += 4;

			index++;
			fprintf(pgmFile,"%d ",data->matrix[index]);

		}
		fprintf(pgmFile,"\n");
	}

	return;
}

/*Opens File*/
FILE* openFile(void) {

	FILE * oFile;
	char name[50];

	printf("What is the filename you would like to open?\n"
			"Ensure the file is in the same directory as the .exe file\n"
			"For example, 'cat.pgm'\n");
	gets(name);

	oFile = fopen (name , "r");			//loading in file
	if (oFile == NULL) {				//checking file is valid
		perror ("Error opening file\n");//prints error message
	}else {
		printf("File successfully Opened.\n");
	}

	return oFile;
}

PGMData* headerFunc(FILE* pFile) {

	int w,h,d;//width, height, depth
	const char s[5] = " \t\r";//define the delimiter for the strtok function
	char *token;

	char header[50];
	int headerArray[100];
	int index = 0;

	for(int i = 0; i < 3; i++) {//for the first 3 lines of the image
		fgets (header , 10 , pFile);
		if(header[strlen(header)-1]=='\n')
			header[strlen(header)-1] = '\0';
		token = strtok(header,s);//parse out the width from the header
		index++;
		headerArray[index] = atoi(token);
		//printf("The value in index %d is %i.\n",index,headerArray[index]);

		token = strtok(NULL, s);
		while( token != NULL )
		   {
				index++;
				headerArray[index] = atoi(token);
				//printf("The value in index %d is %i.\n",index,headerArray[index]);
				token = strtok(NULL, s);
				//getchar();
		   }
	}


	h = headerArray[2];
	w = headerArray[3];
	d = headerArray[4];

	PGMData *data = (PGMData*) malloc(sizeof(PGMData));
	data->row = w;
	data->col = h;
	data->max_gray = d;

	//printf("The width of this image is %d,\nThe height of the image is %d\n"
	//		"The color depth of the image is %d.\n",
	//		data->row,data->col,data->max_gray);

	return data;
}

PGMData* parseFunc(FILE* pFile,PGMData* data) {

	char line[MAX_LENGTH];

		/*allocate space to store tokens for all pixels in the image*/
		data->matrix = (int*)malloc(sizeof(int)*data->row*data->col);

		int index = 0;

		while(index < data->row * data->col) {

			fgets (line, MAX_LENGTH, pFile);

			/*If the last character in 'line' is a new line character,
			 replace it with a null character */
			if(line[strlen(line)-1]=='\n')
				line[strlen(line)-1] = '\0';


			/*define the delimiter for the strtok function*/
			const char s[5] = " \t\r";
			char *token;
			token = strtok(line, s);
			index++;
			/*Starts parsing each pixel into the pointer list of color data*/
			data->matrix[index] = atoi(token);

			//printf("Index: %d value: %d\n",index,data->matrix[index]);

			token = strtok(NULL, s);
			while( token != NULL ){
			/*Parses in the rest of the pixels while token isn't NULL*/
				index++;
				data->matrix[index] = atoi(token);
				//printf("Index: %d value: %d\n",index,data->matrix[index]);

				token = strtok(NULL, s);
			}
			//getchar();
		}

		//printf("You have entered %d tokens.\n", index);
		return data;
}

int errorCheck(PGMData* data,int x, int y){

	if (x < 0 || x > data->row || y < 0 || y > data->col)
		return 1;
	else
		return 0;
}




