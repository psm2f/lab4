
/*
 * Lab4.cpp
 *
 *** Some code segments are taken from Hayri Uğur KOLTUK at
 *** https://ugurkoltuk.wordpress.com/2010/03/04/an-extreme-simple-pgm-io-api/
 *** because after many hours I couldn't figure out how to handle pgm files.
 *
 *  Created on: Feb 15, 2016
 *      Author: Patrick
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LENGTH 100

typedef struct _PGMData {
    int row;
    int col;
    int max_gray;
    int *matrix;
} PGMData;

const char s[5] = " \t\r";//define the delimiter for the strtok function

PGMData* headerFunc(FILE*);
PGMData* parseFunc(FILE*,PGMData*);

int getPix(PGMData*, int, int);
void setPix(PGMData*, int, int, int);
int errorCheck(PGMData*,int, int);

void writeFile(PGMData*,char*);
FILE* openFile(void);

void threshold(PGMData*);
void resize(PGMData*);
void flip(PGMData*);
void shift(PGMData*);

int main() {

	FILE * newFile;

	/*Error check file open*/
	do {
		newFile = openFile();
	}while(newFile == NULL);

	PGMData *data = (PGMData*) malloc(sizeof(PGMData));

	data = headerFunc(newFile);
	data = parseFunc(newFile,data);


	printf("File Threshold...\n");
	threshold(data);

	printf("File Resizing...\n");
	resize(data);


	printf("File Copying.\n");
	char name[] = "Copy.pgm";
	writeFile(data,name);

	printf("File Flipping...\n");
	flip(data);

	printf("File Shifting...\n");
	shift(data);


	getchar();
	fclose (newFile);

	return 0;
}

int getPix(PGMData * data, int x, int y) {

	int value = data->matrix[x*data->col+y];
	return value;
}

void setPix(PGMData * data, int x, int y, int value) {

	data->matrix[x*data->col+y] = value;
	return;
}

void writeFile(PGMData* data, char* name) {

	FILE *pgmFile;
	pgmFile = fopen(name,"w");
	if (pgmFile == NULL) {
		perror("Cannot open file to write");

		getchar();
		exit(EXIT_FAILURE);
	}

	fprintf(pgmFile, "P2 \n");
	fprintf(pgmFile, "%d %d \n", data->col, data->row);
	fprintf(pgmFile, "%d \n", data->max_gray);

	int index = 0;
	int charCounter;

	while( index <= data->col * data->row) {

		charCounter = 0;
		while(charCounter < 70) {

			if(data->matrix[index] < 10)
				charCounter += 2;
			else if(data->matrix[index] < 100)
				charCounter += 3;
			else
				charCounter += 4;

			index++;
			fprintf(pgmFile,"%d ",data->matrix[index]);

		}
		fprintf(pgmFile,"\n");
	}

	return;
}

/*Opens File*/
FILE* openFile(void) {

	FILE * oFile;
	char name[50];

	printf("What is the filename you would like to open?\n"
			"Ensure the file is in the same directory as the .exe file\n"
			"For example, 'cat.pgm'\n");
	gets(name);

	oFile = fopen (name , "r");			//loading in file
	if (oFile == NULL) {				//checking file is valid
		perror ("Error opening file\n");//prints error message
	}else {
		printf("File successfully Opened.\n");
	}

	return oFile;
}

PGMData* headerFunc(FILE* pFile) {

	int w,h,d;//width, height, depth
	const char s[5] = " \t\r";//define the delimiter for the strtok function
	char *token;

	char header[50];
	int headerArray[100];
	int index = 0;

	for(int i = 0; i < 3; i++) {//for the first 3 lines of the image
		fgets (header , 10 , pFile);
		if(header[strlen(header)-1]=='\n')
			header[strlen(header)-1] = '\0';
		token = strtok(header,s);//parse out the width from the header
		index++;
		headerArray[index] = atoi(token);
		//printf("The value in index %d is %i.\n",index,headerArray[index]);

		token = strtok(NULL, s);
		while( token != NULL )
		   {
				index++;
				headerArray[index] = atoi(token);
				//printf("The value in index %d is %i.\n",index,headerArray[index]);
				token = strtok(NULL, s);
				//getchar();
		   }
	}


	h = headerArray[2];
	w = headerArray[3];
	d = headerArray[4];

	PGMData *data = (PGMData*) malloc(sizeof(PGMData));
	data->row = w;
	data->col = h;
	data->max_gray = d;

	//printf("The width of this image is %d,\nThe height of the image is %d\n"
	//		"The color depth of the image is %d.\n",
	//		data->row,data->col,data->max_gray);

	return data;
}

PGMData* parseFunc(FILE* pFile,PGMData* data) {

	char line[MAX_LENGTH];

		/*allocate space to store tokens for all pixels in the image*/
		data->matrix = (int*)malloc(sizeof(int)*data->row*data->col);

		int index = 0;

		while(index < data->row * data->col) {

			fgets (line, MAX_LENGTH, pFile);

			/*If the last character in 'line' is a new line character,
			 replace it with a null character */
			if(line[strlen(line)-1]=='\n')
				line[strlen(line)-1] = '\0';


			/*define the delimiter for the strtok function*/
			const char s[5] = " \t\r";
			char *token;
			token = strtok(line, s);
			index++;
			/*Starts parsing each pixel into the pointer list of color data*/
			data->matrix[index] = atoi(token);

			//printf("Index: %d value: %d\n",index,data->matrix[index]);

			token = strtok(NULL, s);
			while( token != NULL ){
			/*Parses in the rest of the pixels while token isn't NULL*/
				index++;
				data->matrix[index] = atoi(token);
				//printf("Index: %d value: %d\n",index,data->matrix[index]);

				token = strtok(NULL, s);
			}
			//getchar();
		}

		//printf("You have entered %d tokens.\n", index);
		return data;
}

void threshold(PGMData* data) {

	int threshValue;

	do{
		printf("What would you like to be the threshold Value? [0-255]\n");
		scanf("%d", &threshValue);

		if(threshValue < 0 || threshValue > 255)
			printf("Please enter a value between 0 and 255.\n");

	}while(threshValue < 0 || threshValue > 255);

	PGMData * nData = (PGMData*) malloc(sizeof(PGMData));
	nData->matrix = (int*)malloc(sizeof(int)*data->row*data->col);

	nData->row = data->row;
	nData->col = data->col;
	nData->max_gray = data->max_gray;

	for(int x = 0; x < nData->row; x++) {
		for(int y = 0; y < nData->col; y++) {
			if(getPix(data,x,y) <= threshValue)
				setPix(nData,x,y,0);
			else
				setPix(nData,x,y,255);
		}
	}

	printf("Threshold Complete. Writing file.\n");
	char name[] = "Threshold.pgm";
	writeFile(nData,name);

	free(nData->matrix);
	free(nData);
	return;
}

void resize(PGMData* data){


	printf("Resize 1...\n");

	PGMData * nData = (PGMData*) malloc(sizeof(PGMData));

	nData->row = data->row / 2;
	nData->col = data->col / 2;
	nData->max_gray = data->max_gray;

	nData->matrix = (int*)malloc(sizeof(int)*(nData->row*nData->col));

	for(int x = 0; x < nData->row; x++) {
		for(int y = 0; y < nData->col; y++) {
			int val = getPix(data,2*x,2*y);
			setPix(nData,x,y,val);
		}
	}

	printf("Resize 1 Complete. Writing file...\n");
	char name1[] = "resize1.pgm";
	writeFile(nData,name1);


	printf("Resize 2...\n");
	PGMData * mData = (PGMData*) malloc(sizeof(PGMData));

	mData->row = data->row / 2;
	mData->col = data->col / 2;
	mData->max_gray = data->max_gray;

	mData->matrix = (int*)malloc(sizeof(int)*(mData->row*mData->col));

	for(int x = 0; x < mData->row; x++) {
		for(int y = 0; y < mData->col; y++) {
			int pixCount = 0;
			int val = 0;

			//printf("%d - %d",x,y);

			if(errorCheck(data,2*x-1,2*y-1) == 0) {
				val += getPix(data,2*x-1,2*y-1);
				pixCount++;
			}
			if(errorCheck(data,2*x,2*y-1) == 0) {
				val += getPix(data,2*x,2*y-1);
				pixCount++;
			}
			if(errorCheck(data,2*x+1,2*y-1) == 0) {
				val += getPix(data,2*x+1,2*y-1);
				pixCount++;
			}
			if(errorCheck(data,2*x-1,2*y) == 0) {
				val += getPix(data,2*x-1,2*y);
				pixCount++;
			}
			if(errorCheck(data,2*x,2*y) == 0) {
				val += getPix(data,2*x,2*y);
				pixCount++;
			}
			if(errorCheck(data,2*x+1,2*y) == 0) {
				val += getPix(data,2*x+1,2*y);
				pixCount++;
			}
			if(errorCheck(data,2*x-1,2*y+1) == 0) {
				val += getPix(data,2*x-1,2*y+1);
				pixCount++;
			}
			if(errorCheck(data,2*x,2*y+1) == 0) {
				val += getPix(data,2*x,2*y+1);
				pixCount++;
			}
			if(errorCheck(data,2*x+1,2*y+1) == 0) {
				val += getPix(data,2*x+1,2*y+1);
				pixCount++;
			}

			val = val/pixCount;
			setPix(mData,x,y,val);
		}
	}

	printf("Resize 2 Complete. Writing file...\n");
	char name2[] = "resize2.pgm";
	writeFile(mData,name2);

	free(mData->matrix);
	free(mData);
	free(nData->matrix);
	free(nData);
	return;
}

int errorCheck(PGMData* data,int x, int y){

	if (x < 0 || x > data->row || y < 0 || y > data->col)
		return 1;
	else
		return 0;
}

void flip(PGMData* data) {

	printf("Flip...\n");

	PGMData * nData = (PGMData*) malloc(sizeof(PGMData));

	nData->row = data->row;
	nData->col = data->col;
	nData->max_gray = data->max_gray;

	nData->matrix = (int*)malloc(sizeof(int)*(nData->row*nData->col));

	for(int x = 0; x < nData->row; x++) {
		for(int y = 0; y < nData->col; y++) {
			int val = getPix(data,x,data->col - y - 1);
			setPix(nData,x,y,val);
		}
	}

	printf("Flip Complete. Writing file...\n");
	char name1[] = "flip.pgm";
	writeFile(nData,name1);


	free(nData->matrix);
	free(nData);
	return;
}

void shift(PGMData* data) {

	int offset;
	printf("How many pixels would you like to shift the image?");
	scanf("%d",&offset);

	PGMData * nData = (PGMData*) malloc(sizeof(PGMData));

	nData->row = data->row;
	nData->col = data->col;
	nData->max_gray = data->max_gray;

	nData->matrix = (int*)malloc(sizeof(int)*(nData->row*nData->col));

	for(int x = 0; x < nData->row; x++) {
		for(int y = 0; y < nData->col; y++) {
			setPix(nData,x,y,0);
			if(errorCheck(data,x,y-offset) == 0){
				int val = getPix(data,x,y - offset);
				setPix(nData,x,y,val);
			}
		}
	}

	printf("Shift Complete. Writing file...\n");
	char name1[] = "shift.pgm";
	writeFile(nData,name1);


	free(nData->matrix);
	free(nData);
	return;
}




